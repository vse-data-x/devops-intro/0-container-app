# Running a containerized application

1. Install Docker Desktop [CE](https://hub.docker.com/search/?type=edition&offering=community)

2.  Verify installation was succesful by running `docker version` in terminal 

3. Clone this project to your local environment running `git clone https://gitlab.com/vse-data-x/devops-intro/0-container-app``

4. Navigate to the folder you cloned this project 

5. Run `ls` or `dir` depending of your operative system and make sure that `Dockerfile`is present

6. Run `docker image build -t containerapp .`

7. To verify the image was created run `docker images`

8. Run `docker container run -p 9999:8081 containerapp`

9. Open a browser in `localhost:9999`
