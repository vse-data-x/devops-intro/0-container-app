FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
COPY ./app.py /app/app.py 
COPY ./templates /app/templates 
COPY ./static /app/static/
WORKDIR /app 
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
